# Installation 

1. Rename file .env.example menjadi .env dan atur DB sesuai konfigurasi environment Anda.
2. Jalankan `php artisan key:generate`
3. Jalankan `composer install`
4. Jalankan `npm install`
5. Jalankan `gulp`
6. Jalankan `gulp watch` jika berencana melakukan perubahan terhadap css/script
7. Jalankan `php artisan serve`
8. Hack away!