<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="DashBolt is a decision making support system built for mBolt and well, fulfilling Business Intelligences final assignment.">
    <meta name="author" content="BRRR-4 Team">

    <meta property="og:title" content="DashBolt" />
    <meta property="og:type" content="website" />

    <title>DashBolt</title>

    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('/js/smoothscroll.js') }}"></script>


</head>

<body id="landing">

    <div id="headerwrap" class="text-center">
        <h1>DashBolt</h1>
        <h3>A support system built to <b>ease</b> <a href="">mBolt</a> managers' decision making process. <br>Statistics and forecasts are provided <br>to <b>tackle</b> the competition of tomorrows.</h3>
        <div class="row">
            <a href="{{ url('/login') }}" class="btn btn-success">Login</a>
        </div>
    </div><!--/ #headerwrap -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>