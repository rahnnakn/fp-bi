@extends('layouts.app')

@section('htmlheader_title')
  Daftar BTS
@endsection

@section('contentheader_title', 'Daftar BTS')

@section('main-content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Seluruh BTS</h3>
  </div>
  <div class="box-body">
    <div class="dataTable_wrapper table-responsive">
      <table class="table table-bordered table-striped table-hover" id="dataTables">
        <thead>
          <tr class="odd gradeX">
            <th>ID</th>
            <th>City</th>
            <th>Capacity</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($bts as $data)
            <tr>
              <td>{{ $data->id }}</td>
              <td>{{ $data->city }}</td>
              <td>{{ $data->type->capacity }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  @parent
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
  </script>
@endsection