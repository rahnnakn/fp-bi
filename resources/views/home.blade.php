@extends('layouts.app')

@section('htmlheader_title')
	Dashboard
@endsection

@section('contentheader_title', 'Dashboard')

@section('main-content')
<div class="row">
	<div class="col-md-6 connectedSortable ui-sortable">
		<div class="box box-solid">
			<div class="box-header ui-sortable-handle">
				<div class="pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
				<h3 class="box-title">Jumlah Customer & Kapasitas BTS per Daerah</h3>
			</div>
			<div class="box-body">
				<canvas id="bar"></canvas>
			</div>
		</div>
		<div class="box box-solid">
			<div class="box-header ui-sortable-handle">
				<div class="pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
				<h3 class="box-title">Tren Rata-rata Penggunaan Data per Bulan</h3>
			</div>
			<div class="box-body">
				<canvas id="line"></canvas>
			</div>
		</div>
	</div>
	<div class="col-md-6 connectedSortable ui-sortable">	
		<div class="box box-solid">
			<div class="box-header ui-sortable-handle">
				<div class="pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
				<h3 class="box-title">Jumlah Customer per Paket Data</h3>
			</div>
			<div class="box-body">
				<canvas id="doughnut"></canvas>
			</div>
		</div>
		<div class="box box-solid">
			<div class="box-header ui-sortable-handle">
				<div class="pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
				<h3 class="box-title">Target Revenue</h3>
			</div>
			<div class="box-body">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fa fa-usd" aria-hidden="true"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Target Revenue</span>
				   		<span class="info-box-number">Rp {{ number_format($revenue,2,',','.') }} / Rp {{ number_format($target,2,',','.') }}</span>
				    	<!-- The progress section is optional -->
				    	<div class="progress">
				    		<div class="progress-bar" style="width: {{ $percent }}%"></div>
				   		</div>
					 	<span class="progress-description">
					      Rp {{ number_format($target - $revenue,2,',','.') }} to go
					    </span>
					</div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script>
	    $(document).ready(function() {
	        var bar = document.getElementById("bar");
	        var line = document.getElementById("line");
	        var doughnut = document.getElementById("doughnut");

	        var labels = [];
	        var datas = [];
	        var btses = [];
	        var periods = [];
	        var totals = [];
	        var doughnut_label = [];
	        var doughnut_data = [];

	        @foreach ($labels as $label)
	            labels.push('{{ $label }}');
	        @endforeach

	        @foreach ($datas as $data)
	            datas.push('{{ $data }}');
	        @endforeach

	        @foreach ($btses as $bts)
	        	btses.push('{{ $bts->total_capacity }}');
	        @endforeach

	        @foreach ($periods as $period)
	        	periods.push('{{ $period }}');
	        @endforeach

	        @foreach ($totals as $total)
	        	totals.push('{{ $total }}');
	        @endforeach

	        @foreach ($paket as $paketpaket)
	        	doughnut_label.push('{{ $paketpaket->paket }}');
	        	doughnut_data.push('{{ $paketpaket->jumlah }}');
	        @endforeach

	        var dataBar = {
	            labels: labels,
	            datasets: [
	                {
	                    label: "Jumlah User",
	                    backgroundColor: "rgba(255,99,132,0.2)",
	                    borderColor: "rgba(255,99,132,1)",
	                    borderWidth: 1,
	                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
	                    hoverBorderColor: "rgba(255,99,132,1)",
	                    data: datas
	                },
	                {
	                    label: "Kapasitas BTS",
	                    backgroundColor: "rgba(52, 152, 219,.2)",
	                    borderColor: "rgba(31, 58, 147, .4)",
	                    borderWidth: 1,
	                    hoverBackgroundColor: "rgba(52, 152, 219, .4)",
	                    hoverBorderColor: "rgba(31, 58, 147, 1)",
	                    data: btses
	                }
	            ]
	        };

	        var dataLine = {
			    labels: periods,
			    datasets: [
			        {
			            label: "Rata-rata Penggunaan Data",
			            fill: true,
			            lineTension: 0.1,
			            backgroundColor: "rgba(75,192,192,0.4)",
			            borderColor: "rgba(75,192,192,1)",
			            borderCapStyle: 'butt',
			            borderDash: [],
			            borderDashOffset: 0.0,
			            borderJoinStyle: 'miter',
			            pointBorderColor: "rgba(75,192,192,1)",
			            pointBackgroundColor: "#fff",
			            pointBorderWidth: 1,
			            pointHoverRadius: 5,
			            pointHoverBackgroundColor: "rgba(75,192,192,1)",
			            pointHoverBorderColor: "rgba(220,220,220,1)",
			            pointHoverBorderWidth: 2,
			            pointRadius: 1,
			            pointHitRadius: 10,
			            data: totals,
			        }
			    ]
			};

			var dataDoughnut = {
			    labels: doughnut_label,
			    datasets: [
			        {
			            data: doughnut_data,
			            backgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#E38888",
			                "#82D5FF"
			            ],
			            hoverBackgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#E38888",
			                "#82D5FF"
			            ]
			        }]
			};

	        var optionBar = {
	            scales: {
	                yAxes: [{
	                    ticks: {
	                        beginAtZero:true
	                    }
	                }]
	            }
	        };

	        var optionLine = {
	        	scales: {
	        		xAxes: [{
	        			display: true,
	                    ticks: {
	                        beginAtZero:true
	                    }
	                }],
	                yAxes: [{
	                    ticks: {
	                        beginAtZero:true
	                    }
	                }]
	            }
		    }

		    var optionDoughnut = {
		       
		    }

	        var myBarChart = new Chart(bar, {
	            type: 'bar',
	            data: dataBar,
	            options: optionBar,
	            responsive: true,
    			maintainAspectRatio: true
	        });

	        var myLineChart = new Chart(line, {
			    type: 'line',
			    data: dataLine,
			    options: optionLine
			});

			var myDoughnutChart = new Chart(doughnut, {
			    type: 'doughnut',
			    data: dataDoughnut,
			    options: optionDoughnut
			});

			myBarChart.resize();
			myLineChart.resize();
			myDoughnutChart.resize();

	    });
	</script>
@endsection
