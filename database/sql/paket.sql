CREATE TABLE paket (
  id mediumint(8) unsigned NOT NULL auto_increment,
  paketName varchar(255),
  quota varchar(255),
  paketPrice int NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO paket (id,paketName,quota,paketPrice) VALUES (1,"Spark","13",150000),(2,"Lightning","17",200000),(3,"Thunder","55",600000),(4,"Plasma","110",1200000);
