CREATE TABLE `bts` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `city` varchar(255),
  `tipe` varchar(255),
  PRIMARY KEY (`id`)
);

INSERT INTO `bts` (`id`,`city`,`tipe`) VALUES (1,"Bekasi","Y"),(2,"Depok","X"),(3,"Depok","Y"),(4,"Bekasi","X"),(5,"Bogor","X"),(6,"Bogor","Y"),(7,"Bogor","X"),(8,"Bogor","Y"),(9,"Depok","X"),(10,"Bogor","Y"),(11,"Bekasi","Y"),(12,"Depok","X"),(13,"Jakarta","Y"),(14,"Depok","Y"),(15,"Jakarta","X"),(16,"Jakarta","X"),(17,"Jakarta","Y"),(18,"Jakarta","Y"),(19,"Bekasi","X"),(20,"Tangerang","X"),(21,"Bogor","X"),(22,"Depok","X"),(23,"Tangerang","X"),(24,"Bogor","X"),(25,"Bogor","X"),(26,"Bogor","Y"),(27,"Bogor","Y"),(28,"Depok","Y"),(29,"Depok","X"),(30,"Depok","X");
INSERT INTO `bts` (`id`,`city`,`tipe`) VALUES (31,"Jakarta","X"),(32,"Tangerang","X"),(33,"Jakarta","X"),(34,"Tangerang","X"),(35,"Bogor","X"),(36,"Tangerang","X"),(37,"Bogor","Y"),(38,"Bekasi","X"),(39,"Bogor","X"),(40,"Bogor","X"),(41,"Depok","Y"),(42,"Bekasi","X"),(43,"Depok","Y"),(44,"Tangerang","X"),(45,"Tangerang","Y"),(46,"Tangerang","Y"),(47,"Bogor","Y"),(48,"Depok","Y"),(49,"Tangerang","Y"),(50,"Bogor","Y"),(51,"Bogor","Y"),(52,"Bekasi","X"),(53,"Depok","Y"),(54,"Bogor","X"),(55,"Depok","Y"),(56,"Tangerang","Y"),(57,"Depok","Y"),(58,"Bogor","X"),(59,"Tangerang","X"),(60,"Bogor","X");
INSERT INTO `bts` (`id`,`city`,`tipe`) VALUES (61,"Depok","Y"),(62,"Depok","X"),(63,"Depok","Y"),(64,"Depok","X"),(65,"Bogor","Y"),(66,"Jakarta","X"),(67,"Bogor","X"),(68,"Bekasi","Y"),(69,"Jakarta","X"),(70,"Bogor","X"),(71,"Bogor","Y"),(72,"Bekasi","Y"),(73,"Bogor","Y"),(74,"Bogor","Y"),(75,"Bekasi","X"),(76,"Bekasi","X"),(77,"Tangerang","X"),(78,"Jakarta","X"),(79,"Jakarta","Y"),(80,"Jakarta","Y"),(81,"Bogor","X"),(82,"Jakarta","X"),(83,"Jakarta","X"),(84,"Depok","X"),(85,"Bekasi","X"),(86,"Tangerang","X"),(87,"Bogor","Y"),(88,"Bogor","X"),(89,"Bekasi","Y"),(90,"Tangerang","Y");
INSERT INTO `bts` (`id`,`city`,`tipe`) VALUES (91,"Depok","Y"),(92,"Bekasi","X"),(93,"Bogor","Y"),(94,"Tangerang","X"),(95,"Bogor","X"),(96,"Jakarta","X"),(97,"Bogor","Y"),(98,"Bogor","X"),(99,"Bekasi","X"),(100,"Tangerang","Y");
