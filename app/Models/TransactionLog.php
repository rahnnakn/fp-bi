<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TransactionLog
 */
class TransactionLog extends Model
{
    protected $table = 'transactionLog';

    public $timestamps = false;

    protected $fillable = [
        'custID',
        'date',
        'dataTotal'
    ];

    protected $guarded = [];

        
}