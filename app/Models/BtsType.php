<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BtsType
 */
class BtsType extends Model
{
    protected $table = 'btsType';

    protected $primaryKey = 'tipe';

	public $timestamps = false;

    protected $fillable = [
        'capacity'
    ];

    protected $guarded = [];

        
}