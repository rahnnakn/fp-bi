<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaketLog
 */
class PaketLog extends Model
{
    protected $table = 'paketLog';

    public $timestamps = false;

    protected $fillable = [
        'paketId',
        'custID',
        'purchaseDate'
    ];

    protected $guarded = [];

        
}