<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 */
class Customer extends Model
{
    protected $table = 'customer';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'gender',
        'telephone',
        'birthdate',
        'address',
        'city',
        'paket',
        'subscriptionStatus'
    ];

    protected $guarded = [];

        
}