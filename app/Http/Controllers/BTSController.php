<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Bts;
use View;

class BTSController extends Controller
{
    public function index()
    {
    	$bts = Bts::paginate(30);

    	return view('bts_list', compact('bts'));

    	// return redirect()->route('bts.viewList', $bts);
    }
}
