<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Customer;
use App\Models\Bts;
use App\Models\TransactionLog;
use App\Models\Paket;
use App\Models\PaketLog;
use DB;

class DashboardController extends Controller
{
    public function index() 
    {
    	$query_data = Customer::select(DB::raw('count(name) as count'))->where('subscriptionStatus','active')->groupBy('city')->get();
    	$query_label = Customer::select('city')->distinct()->orderBy('city','asc')->get();
    	$query_bts = Bts::join('btsType','bts.tipe','=','btsType.tipe')->select(DB::raw('city,sum(capacity) as total_capacity'))->groupBy('city')->get();
    	$query_trans = TransactionLog::select(DB::raw('extract(year from tDate) as tyear, extract(month from tDate) as tmonth, sum(dataTotal)/count(*) as total'))->groupBy('tyear')->groupBy('tmonth')->orderBy('tyear','asc')->get();

        $query_paket = Customer::select(DB::raw('paketSub as paket, count(*) as jumlah'))->where('subscriptionStatus', 'Active')->groupBy('paketSub')->get();

        $query_revenue = DB::select('SELECT month(paketlog.purchaseDate) as pMonth, year(paketlog.purchaseDate) as pYear, sum(paketPrice) as total FROM paketlog, paket WHERE paketId = paket.id AND year(purchaseDate)= year(now()) AND month(purchaseDate) = month(now()) group by pMonth,pYear');

        $query_target = DB::select('SELECT tTarget FROM `target` WHERE tBulan = month(now()) AND tTahun = year(now())');
    	
    	$labels = [];
    	$datas = [];
    	$btses = [];
    	$periods = [];
    	$totals = [];
        $paket = [];
        $revenue = $query_revenue[0]->total;
        $target = $query_target[0]->tTarget;
        $percent = $revenue/$target*100;

    	foreach($query_data as $data){
    		array_push($datas, $data->count);
    	}

    	foreach($query_label as $label){
    		array_push($labels, $label->city);
    	}

    	foreach($query_bts as $bts){
    		array_push($btses, $bts);
    	}

    	foreach($query_trans as $trans){
    		$value = $trans->tyear . '/' . $trans->tmonth;
    		array_push($periods, $value);
    		array_push($totals, $trans->total);
    	}

        foreach($query_paket as $paket_instance){
            array_push($paket, $paket_instance);
        }
    	
    	return view('home', compact(['labels','datas','btses','periods','totals', 'paket', 'revenue', 'target', 'percent']));
    }
}
