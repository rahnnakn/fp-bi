<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'loggedInRedirect'], function(){
	Route::get('/', function () {
	    return view('welcome');
	});
});

Route::group(['middleware' => 'auth'], function(){
	Route::get('/dashboard', [
		'as' => 'dashboard', 'uses' => 'DashboardController@index'
		]);

	Route::get('/bts/getList', [
		'as' => 'bts.list', 'uses' => 'BTSController@index'
		]);

	Route::get('/bts/list', [
		'as' => 'bts.viewList', function () {return view('bts_list');}
		]);
});